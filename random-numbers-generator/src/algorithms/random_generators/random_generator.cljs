(ns algorithms.random-generators.random-generator)

(defprotocol RandomGenerator
  (generate [this]))
