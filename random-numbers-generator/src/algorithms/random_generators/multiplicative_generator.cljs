(ns algorithms.random-generators.multiplicative-generator
  (:require [algorithms.random-generators.random-generator :refer [RandomGenerator]]))

(deftype MultiplicativeGenerator [^:mutable x lambda mu m]
  RandomGenerator
  (generate [this]
    (let [prev-x x
          new-x (-> x (* lambda) (+ mu) (rem m))]
      (set! x new-x)
      (/ x m))))

(defn multiplicative-generator [x lambda mu m]
  (MultiplicativeGenerator. x lambda mu m))
