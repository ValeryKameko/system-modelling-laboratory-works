(ns algorithms.random-generators.lemer-generator
  (:require [algorithms.random-generators.multiplicative-generator :refer [multiplicative-generator]]))

(defn lemer-generator [r a m]
  (multiplicative-generator r a 0 m))
