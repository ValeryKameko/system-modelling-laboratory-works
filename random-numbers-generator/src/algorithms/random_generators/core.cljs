(ns algorithms.random-generators.core
  (:require [algorithms.random-generators.random-generator :as random-generator]
            [algorithms.random-generators.multiplicative-generator :as multiplicative-generator]
            [algorithms.random-generators.lemer-generator :as lemer-generator]))
