(ns algorithms.uniformity-estimators
  (:require [algorithms.random-generators.core]))

(defn- square [x]
  (* x x))

(defn calc-quadrature-value [xs]
  (let [pairs (partition 2 xs)
        match-count (->> pairs
                         (filter #(< (reduce + (map square %)) 1))
                         count)]
    (/ (* 2 match-count) (count xs))))

(defn- calc-period-info- [indices indexed-numbers]
  (loop [indices indices
         indexed-numbers indexed-numbers]
    (when-first [[index number] indexed-numbers]
      (if-let [last-index (get indices (str number))]
        {:period (- index last-index)
          :aperiod-prefix index}
        (recur (assoc! indices (str number) index) (rest indexed-numbers))))))

(defn calc-period-info [xs]
  (let [indexed-xs (map-indexed list xs)
        length (count xs)]
    (if-let [period-info (calc-period-info- (transient (hash-map)) indexed-xs)]
      period-info
      {:period length
       :aperiod-prefix length})))