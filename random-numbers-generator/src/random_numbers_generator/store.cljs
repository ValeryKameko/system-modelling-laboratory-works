(ns random-numbers-generator.store
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.dom :as rdom]
            [cljs.core.async :refer [<!]]
            [algorithms.random-estimators :refer [calc-mean calc-dispersion calc-standard-deviation]]
            [algorithms.uniformity-estimators :refer [calc-quadrature-value calc-period-info]]
            [algorithms.random-generators.random-generator :refer [generate]]
            [algorithms.random-generators.lemer-generator :refer [lemer-generator]])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(def ^:const min-float-value -100.0)
(def ^:const max-float-value 100.0)

(def ^:const max-int-value (Math/pow 2 30))
(def ^:const m-range [0 max-int-value])
(def ^:const N-range [0 100000])

(def store (atom 
  {:parameters {:a 0
                :r0 1
                :m 10
                :N 100
                :min 0.0
                :max 1.0}
   :data {:xs []}
   :estimations nil
   :uniformity-estimations nil
   :ui {:bars-count 20
        :generating false}}))

(defn get-estimations []
  (-> @store :estimations))

(defn set-estimations [estimations]
  (swap! store assoc :estimations estimations))

(defn get-uniformity-estimations []
  (-> @store :uniformity-estimations))

(defn set-uniformity-estimations [uniformity-estimations]
  (swap! store assoc :uniformity-estimations uniformity-estimations))

(defn set-xs [xs]
  (swap! store assoc-in [:data :xs] xs))

(defn get-xs []
  (-> @store :data :xs))

(defn on-change-parameter []
  (set-xs nil)
  (set-estimations nil)
  (set-uniformity-estimations nil))

(defn get-bars-count []
  (-> @store :ui :bars-count))

(defn set-bars-count [bars-count]
  (swap! store assoc-in [:ui :bars-count] bars-count))

(defn set-a [a]
  (do
    (on-change-parameter)
    (swap! store assoc-in [:parameters :a] a)))

(defn get-a []
  (-> @store :parameters :a))

(defn set-m [m]
  (do
    (on-change-parameter)
    (swap! store assoc-in [:parameters :m] m)))

(defn get-m []
  (-> @store :parameters :m))

(defn set-r0 [r0]
  (do
    (on-change-parameter)
    (swap! store assoc-in [:parameters :r0] r0)))

(defn get-r0 []
  (-> @store :parameters :r0))

(defn r0-range []
  (let [m (get-m)]
    [0 m]))

(defn a-range []
  (let [m (get-m)]
    [0 m]))

(defn set-N [N]
  (do
    (on-change-parameter)
    (swap! store assoc-in [:parameters :N] N)))

(defn get-N []
  (-> @store :parameters :N))

(defn set-min [min]
  (swap! store assoc-in [:parameters :min] min))

(defn get-min []
  (-> @store :parameters :min))

(defn set-max [max]
  (swap! store assoc-in [:parameters :max] max))

(defn get-max []
  (-> @store :parameters :max))

(defn min-range []
  (let [max (get-max)]
    [min-float-value max]))

(defn max-range []
  (let [min (get-min)]
    [min max-float-value]))

(defn get-generating []
  (-> @store :ui :generating))

(defn set-generating [generating]
  (swap! store assoc-in [:ui :generating] generating))

(defn- calc-estimations [xs]
  (let [mean (calc-mean xs)
        dispersion (calc-dispersion xs)
        standard-deviation (calc-standard-deviation xs)]
    {:m mean
     :D dispersion
     :sigma standard-deviation}))

(defn- calc-uniformity-estimations [xs]
  (let [quadrature-value (calc-quadrature-value xs)
        period-info (calc-period-info xs)]
    {:P (:period period-info)
     :L (:aperiod-prefix period-info)
     :Q quadrature-value}))

(defn- generate-values [generator N min max]
  (let [xs (repeatedly N #(generate generator))]
    (map #(-> %
              (* (- max min))
              (+ min))
         xs)))

(defn generate-xs []
  (let [r0 (js/parseInt (get-r0))
        a (js/parseInt (get-a))
        m (js/parseInt (get-m))
        N (js/parseInt (get-N))
        generator (lemer-generator r0 a m)
        min (get-min)
        max (get-max)]
    (set-generating true)
    (go
      (set-xs (generate-values generator N min max))
      (-> (get-xs) calc-estimations set-estimations)
      (-> (get-xs) calc-uniformity-estimations set-uniformity-estimations)
      (set-generating false))))
