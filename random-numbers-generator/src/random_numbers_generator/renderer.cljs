(ns random-numbers-generator.renderer
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.dom :as rdom]
            [reagent.format :as fmt]
            ["react" :as react]
            [random-numbers-generator.semantic-ui :as sui]
            [random-numbers-generator.charts :refer [histogram-component]]
            [random-numbers-generator.store :as store]))

(def float-format "%.10f")
(def int-format "%d")

(defn- format-number [format x]
  (or
    (some->> x (fmt/format format))
    "—"))
            
(defn- controls-component []
  [:> (sui/component "Segment.Group") {:raised true}
    [:> (sui/component "Segment")
      [:> (sui/component "Header") {:as "h3"
                                    :textAlign "center"}
      "Controls"]]
    [:> (sui/component "Segment")
      [:> (sui/component "Header") {:as "h4"}
        "Lemer algorithm coefficients"]
      [:> (sui/component "Form")
        [:> (sui/component "Form.Field")
          [:label "R0"]
          [:> sui/NumberInput {:value (str (store/get-r0))
                               :onChange #(store/set-r0 %)
                               :minValue (first (store/r0-range))
                               :maxValue (second (store/r0-range))
                               :valueType "integer"
                               :size "mini"}]]]
      [:> (sui/component "Form")
        [:> (sui/component "Form.Field")
          [:label "a"]
          [:> sui/NumberInput {:value (str (store/get-a))
                               :onChange #(store/set-a %)
                               :minValue (first (store/a-range))
                               :maxValue (second (store/a-range))
                               :valueType "integer"
                               :size "mini"}]]]
      [:> (sui/component "Form")
        [:> (sui/component "Form.Field")
          [:label "m"]
          [:> sui/NumberInput {:value (str (store/get-m))
                               :onChange #(store/set-m %)
                               :minValue (first store/m-range)
                               :maxValue (second store/m-range)
                               :valueType "integer"
                               :size "mini"}]]]]
    [:> (sui/component "Segment")
      [:> (sui/component "Header") {:as "h4"}
        "Generating parameters"]
      [:> (sui/component "Form")
        [:> (sui/component "Form.Field")
          [:label "N (count)"]
          [:> sui/NumberInput {:value (str (store/get-N))
                               :onChange #(store/set-N %)
                               :minValue (first store/N-range)
                               :maxValue (second store/N-range)
                               :valueType "integer"
                               :size "mini"}]]]]
    [:> (sui/component "Segment") {:textAlign "center"}
      [:> (sui/component "Button") {:onClick store/generate-xs
                                    :disabled (store/get-generating)
                                    :loading (store/get-generating)}
          "Generate"]]])

(defn- estimations-component []
  [:> (sui/component "Segment.Group") {:raised true}
    [:> (sui/component "Segment")
      [:> (sui/component "Header") {:as "h4"
                                    :textAlign "center"}
        "Estimations"]]
    [:> (sui/component "Segment")
      [:> (sui/component "Table")
        [:> (sui/component "Table.Body")
          [:> (sui/component "Table.Row")
            [:> (sui/component "Table.Cell") {:width 1}
              [:> (sui/component "Header") {:as "h5"
                                            :textAlign "center"}
                "m (mean)"]]
            [:> (sui/component "Table.Cell") {:textAlign "center"
                                              :width 5}
              (format-number float-format (some->> (store/get-estimations) :m))]]
          [:> (sui/component "Table.Row")
            [:> (sui/component "Table.Cell") {:width 1}
              [:> (sui/component "Header") {:as "h5"
                                            :textAlign "center"}
                "D (dispersion)"]]
            [:> (sui/component "Table.Cell") {:textAlign "center"
                                              :width 5}
              (format-number float-format (some->> (store/get-estimations) :D))]]
          [:> (sui/component "Table.Row")
            [:> (sui/component "Table.Cell") {:width 1}
              [:> (sui/component "Header") {:as "h5"
                                            :textAlign "center"}
                "σ (standard deviation)"]]
            [:> (sui/component "Table.Cell") {:textAlign "center"
                                              :width 5}
              (format-number float-format (some->> (store/get-estimations) :sigma))]]]]]])

(defn- uniformity-estimations-component []
  [:> (sui/component "Segment.Group") {:raised true}
    [:> (sui/component "Segment")
      [:> (sui/component "Header") {:as "h4"
                                    :textAlign "center"}
        "Uniformity estimations"]]
    [:> (sui/component "Segment")
      [:> (sui/component "Table")
        [:> (sui/component "Table.Body")
          [:> (sui/component "Table.Row")
            [:> (sui/component "Table.Cell") {:width 1}
              [:> (sui/component "Header") {:as "h5"
                                            :textAlign "center"}
                "P (period)"]]
            [:> (sui/component "Table.Cell") {:textAlign "center"
                                              :width 5}
              (format-number int-format (some->> (store/get-uniformity-estimations) :P))]]
          [:> (sui/component "Table.Row")
            [:> (sui/component "Table.Cell") {:width 1}
              [:> (sui/component "Header") {:as "h5"
                                            :textAlign "center"}
                "L (aperiodic prefix)"]]
            [:> (sui/component "Table.Cell") {:textAlign "center"
                                              :width 5}
              (format-number int-format (some->> (store/get-uniformity-estimations) :L))]]
          [:> (sui/component "Table.Row")
            [:> (sui/component "Table.Cell") {:width 1}
              [:> (sui/component "Header") {:as "h5"
                                            :textAlign "center"}
                "2K / N"]]
            [:> (sui/component "Table.Cell") {:textAlign "center"
                                              :width 5}
              (format-number float-format (some->> (store/get-uniformity-estimations) :Q))]]]]]])

(defn root-component []
  [:div {:style {:margin "16px"}}
    [:> (sui/component "Grid") {:columns 2}
      [:> (sui/component "Grid.Row")
        [:> (sui/component "Grid.Column") {:width 12}
          [histogram-component (store/get-xs)
                               (store/get-min)
                               (store/get-max)
                               (store/get-bars-count)]]
        [:> (sui/component "Grid.Column") {:width 4}
          [controls-component]
          [estimations-component]
          [uniformity-estimations-component]]]]])

(rdom/render [root-component]
             (js/document.getElementById "app-container"))
