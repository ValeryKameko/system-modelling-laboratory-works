(ns random-numbers-generator.charts
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.dom :as rdom]
            [reagent.format :as fmt]
            ["recharts" :as recharts]
            [thi.ng.color.core :as col]
            [random-numbers-generator.store :as store]))

(def fill-color @(-> (col/hsva 0.1 0.7 1.0 0.5) col/as-int24 col/as-css))

(def stroke-color @(-> (col/hsva 0.1 0.2 1.0) col/as-int24 col/as-css))

(defn- calc-bar [i frequency mn mx bars-count]
  (let [calc-bar-end #(-> % (/ bars-count) (* (- mx mn)) (+ mn))
        bar-range [(calc-bar-end i) (calc-bar-end (+ i 1))]]
    [bar-range frequency]))

(defn- calc-bars [xs mn mx bars-count]
  (let [len (max (count xs) 1)
        delta (/ (- mx mn) bars-count)
        calc-bar-index #(-> % (- mn) (/ delta) Math/trunc)
        bars-map (->> xs (map calc-bar-index) frequencies)
        bar-indices (range bars-count)
        calc-frequency #(/ (get bars-map % 0) len)]
      (into (sorted-map) (map #(calc-bar % (calc-frequency %) mn mx bars-count) bar-indices))))

(defn- calc-histogram-data-element [range frequency]
  {:x (fmt/format "(%.2f, %.2f)" (nth range 0) (nth range 1))
   :frequency frequency})

(defn- calc-histogram-data [xs mn mx bars-count]
  (let [bars (calc-bars xs mn mx bars-count)]
    (map #(apply calc-histogram-data-element %) bars)))

(defn ^:exports histogram-component [xs mn mx bars-count]
  (let [data (clj->js (calc-histogram-data xs mn mx bars-count))]
    [:> recharts/ResponsiveContainer {:aspect (/ 16 9)
                                      :debounce 0}
      [:> recharts/BarChart {:data data
                             :barCategoryGap "0%"}
        [:> recharts/CartesianGrid {:strokeDashArray "3 3"}]
        [:> recharts/XAxis {:dataKey "x"}]
        [:> recharts/YAxis]
        [:> recharts/Tooltip]
        [:> recharts/Legend]
        [:> recharts/Bar {:dataKey "frequency"
                          :fill fill-color
                          :stroke stroke-color}]]]))
