﻿using System;
using System.Linq;

namespace SimpleQueueSimulator.Lib.JobWorkers
{
    public class SiftedJobWorker : JobWorker
    {
        private readonly Random _random;
        private readonly double _siftProbability;

        private bool _isJobDone = false;

        public SiftedJobWorker(Random random, double siftProbability)
        {
            _random = random;
            _siftProbability = siftProbability;
        }

        public override bool TryInsert(IJob job)
        {
            var insert = base.TryInsert(job);
            if (insert)
                _isJobDone = false;
            return insert;
        }

        public override bool CanExtract() =>
            _isJobDone && base.CanExtract();

        public override bool TryExtract(out IJob? job)
        {
            var extract = CanExtract();
            job = extract ? ExtractJob() : null;
            if (extract)
                _isJobDone = false;
            return extract;
        }

        public override void OnEndTick()
        {
            base.OnEndTick();
            SampleIsJobDone();
        }

        private void SampleIsJobDone() =>
            _isJobDone |= _random.NextDouble() > _siftProbability;

        public override string ToString() => 
            $"{Jobs.Count()}";
    }
}