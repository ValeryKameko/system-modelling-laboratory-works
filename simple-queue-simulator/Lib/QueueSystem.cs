﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SimpleQueueSimulator.Lib
{
    public class QueueSystem
    {
        private readonly List<IQueueSystemElement> _elements = new List<IQueueSystemElement>();

        private Dictionary<IQueueSystemElement, List<QueueSystemElementEdge>> _forwardEdges =
            new Dictionary<IQueueSystemElement, List<QueueSystemElementEdge>>();

        private Dictionary<IQueueSystemElement, List<QueueSystemElementEdge>> _backwardEdges =
            new Dictionary<IQueueSystemElement, List<QueueSystemElementEdge>>();

        public QueueSystem()
        {
        }

        public void InsertElement(IQueueSystemElement element) =>
            _elements.Add(element);

        public void InsertEdge(QueueSystemElementEdge edge)
        {
            _forwardEdges.TryAdd(edge.From, new List<QueueSystemElementEdge>());
            _forwardEdges[edge.From].Add(edge);

            _backwardEdges.TryAdd(edge.To, new List<QueueSystemElementEdge>());
            _backwardEdges[edge.To].Add(edge);
        }

        private IComparer<IQueueSystemElement> CreateElementComparer() =>
            Comparer<IQueueSystemElement>.Create(
                (element, otherElement) => _elements.IndexOf(element) - _elements.IndexOf(otherElement)
            );

        public void ProcessTick(JobStat jobStat)
        {
            var elementsComparer = CreateElementComparer();
            var processingElements = new SortedSet<IQueueSystemElement>(_elements, elementsComparer);
            var processedElements = new SortedSet<IQueueSystemElement>(elementsComparer);
            
            foreach (var element in processingElements)
            {
                if (element is JobWorker)
                {
                    if (element.TryExtract(out var job))
                    {
                        jobStat.CumulativeProcessedJobs++;
                        jobStat.AddJob(job as Job);
                    }
                }
            }

            while (processingElements.Count > 0 &&
                   processingElements.TryGetValue(processingElements.First(), out var processingElement))
            {
                processingElements.Remove(processingElement);
                
                var isJobMoved = false;

                if (!_forwardEdges.TryGetValue(processingElement, out var forwardEdges))
                    forwardEdges = new List<QueueSystemElementEdge>();

                if (!_backwardEdges.TryGetValue(processingElement, out var backwardEdges))
                    backwardEdges = new List<QueueSystemElementEdge>();

                foreach (var edge in forwardEdges.Concat(backwardEdges))
                {
                    var from = edge.From;
                    var to = edge.To;

                    if (from.CanExtract() && to.CanInsert())
                        isJobMoved |= from.TryExtract(out var job) && to.TryInsert(job);
                }

                if (isJobMoved)
                {
                    foreach (var forwardEdge in forwardEdges)
                        if (processedElements.TryGetValue(forwardEdge.To, out var forwardEdgeTo))
                        {
                            processedElements.Remove(forwardEdgeTo);
                            processingElements.Add(forwardEdgeTo);
                        }

                    foreach (var backwardEdge in backwardEdges)
                        if (processedElements.TryGetValue(backwardEdge.From, out var backwardEdgeFrom))
                        {
                            processedElements.Remove(backwardEdgeFrom);
                            processingElements.Add(backwardEdgeFrom);
                        }

                    processingElements.Add(processingElement);
                }
                else
                {
                    processedElements.Add(processingElement);
                }
            }

            bool isBlocked = false;
            foreach (var element in processedElements)
            {
                isBlocked |= element is JobSource && element.CanExtract();
                if (element is JobWorker)
                {
                    jobStat.CumulativeJobsCount += element.Jobs.Count();
                }

                if (element is JobQueue)
                {
                    jobStat.CumulativeJobsCount += element.Jobs.Count();
                    jobStat.CumulativeQueueLength += element.Jobs.Count();
                }

                element.OnEndTick();
            }

            jobStat.TicksCount++;
            if (isBlocked)
                jobStat.BlockedTicksCount++;
        }
    }
}