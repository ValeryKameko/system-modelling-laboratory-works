﻿namespace SimpleQueueSimulator.Lib
{
    public class QueueSystemElementEdge
    {
        public readonly EdgeType Type;
        public readonly IQueueSystemElement From;
        public readonly IQueueSystemElement To;

        public enum EdgeType
        {
            Block,
            Discard,
        }

        public QueueSystemElementEdge(EdgeType type, IQueueSystemElement from, IQueueSystemElement to)
        {
            Type = type;
            From = from;
            To = to;
        }
    }
}