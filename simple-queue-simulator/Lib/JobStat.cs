﻿namespace SimpleQueueSimulator.Lib
{
    public class JobStat
    {
        public int JobsCount { get; private set; } = 0;

        public int LifetimeTicks { get; private set; } = 0;

        public int BlockTicks { get; private set; } = 0;

        public int WaitTicks { get; private set; } = 0;

        public int ProcessTicks { get; private set; } = 0;

        public int DiscardedJobsCount { get; private set; } = 0;

        public int CumulativeProcessedJobs = 0;
        
        public int CumulativeJobsCount = 0;
        
        public int CumulativeQueueLength = 0;
        
        public int TicksCount = 0;

        public int BlockedTicksCount = 0;

        public void AddJob(Job job)
        {
            ++JobsCount;
            LifetimeTicks += job.LifetimeTicks;
            BlockTicks += job.BlockTicks;
            WaitTicks += job.WaitTicks;
            ProcessTicks += job.ProcessTicks;
            if (job.IsDiscarded)
                ++DiscardedJobsCount;
        }

        public double BlockProbability => BlockedTicksCount / (double) TicksCount;
        
        public double AverageQueueLength => CumulativeQueueLength / (double) TicksCount;
        
        public double AverageJobsCount => CumulativeJobsCount / (double) TicksCount;
        
        public double AverageThroughput => CumulativeProcessedJobs / (double) TicksCount;

        public double AverageWaitTime => WaitTicks / (double) JobsCount;
        
        public double AverageLifetimeTime => LifetimeTicks / (double) JobsCount;
    }
}