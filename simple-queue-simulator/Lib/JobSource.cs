﻿using System;
using System.Collections.Generic;

namespace SimpleQueueSimulator.Lib
{
    public abstract class JobSource : IQueueSystemElement
    {
        public abstract IEnumerable<IJob> Jobs { get; }

        protected readonly Func<IJob> JobFactory;

        public JobSource(Func<IJob> jobFactory) =>
            JobFactory = jobFactory;

        public bool CanInsert() =>
            false;

        public bool TryInsert(IJob job) =>
            false;

        public abstract bool CanExtract();

        public abstract bool TryExtract(out IJob? job);

        public virtual void OnEndTick()
        {
        }
    }
}