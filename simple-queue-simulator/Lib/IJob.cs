﻿namespace SimpleQueueSimulator.Lib
{
    public interface IJob
    {
        void Live();
        
        void Discard();
        
        void Block();


        void Wait();

        void Process();
    }
}