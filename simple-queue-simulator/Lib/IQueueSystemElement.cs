﻿using System.Collections.Generic;

namespace SimpleQueueSimulator.Lib
{
    public interface IQueueSystemElement
    {
        IEnumerable<IJob> Jobs { get; }

        public bool CanInsert();
        public bool TryInsert(IJob job);

        public bool CanExtract();
        public bool TryExtract(out IJob? job);

        public void OnEndTick()
        {
        }
    }
}