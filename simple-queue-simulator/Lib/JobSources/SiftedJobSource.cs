﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleQueueSimulator.Lib.JobSources
{
    public class SiftedJobSource : JobSource
    {
        private readonly Random _random;
        private readonly float _siftProbability;

        private IJob? job = null;

        public override IEnumerable<IJob> Jobs => job == null ? new[] {job!} : Enumerable.Empty<IJob>();

        public SiftedJobSource(Random random, float siftProbability, Func<IJob> jobFactory) : base(jobFactory)
        {
            _random = random;
            _siftProbability = siftProbability;

            SampleJobGenerated();
        }

        public override bool CanExtract()
            => job != null;

        public override bool TryExtract(out IJob? job)
        {
            var extract = CanExtract();
            job = extract ? this.job : null;

            if (extract)
                this.job = null;

            return extract;
        }

        public override void OnEndTick() =>
            SampleJobGenerated();

        private void SampleJobGenerated() =>
            job ??= _random.NextDouble() > _siftProbability ? JobFactory.Invoke() : null;
    }
}