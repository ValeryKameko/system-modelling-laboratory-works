﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleQueueSimulator.Lib.JobSources
{
    public class FixedTicksJobSource : JobSource
    {
        private readonly int _intervalTicks;
        private int _remainingTicks;

        private IJob? job = null;

        public override IEnumerable<IJob> Jobs => job == null ? new[] {job!} : Enumerable.Empty<IJob>();

        public FixedTicksJobSource(int intervalTicks, Func<IJob> jobFactory) : base(jobFactory)
        {
            _intervalTicks = intervalTicks;
            _remainingTicks = intervalTicks;
        }

        public override bool CanExtract()
            => job != null;

        public override bool TryExtract(out IJob? job)
        {
            var extract = CanExtract();
            job = extract ? this.job : null;

            if (extract)
            {
                this.job = null;
                _remainingTicks = _intervalTicks + 1;
            }

            return extract;
        }

        public override void OnEndTick()
        {
            if (_remainingTicks > 0)
                _remainingTicks--;

            if (_remainingTicks <= 1 && job == null)
                job = JobFactory.Invoke();
        }

        public override string ToString()
        {
            return $"{_remainingTicks}";
        }
    }
}