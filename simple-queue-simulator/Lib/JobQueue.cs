﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net.Mail;

namespace SimpleQueueSimulator.Lib
{
    public class JobQueue : IQueueSystemElement
    {
        public IEnumerable<IJob> Jobs => _jobs;

        private readonly int _capacity;
        private Queue<IJob> _jobs;

        public JobQueue(int capacity)
        {
            _capacity = capacity;
            _jobs = new Queue<IJob>(_capacity);
        }

        public bool CanInsert() =>
            _jobs.Count < _capacity;

        public bool TryInsert(IJob job)
        {
            var inserted = CanInsert();
            if (inserted)
                _jobs.Enqueue(job);

            return inserted;
        }

        public bool CanExtract() =>
            _jobs.Count > 0;

        public bool TryExtract(out IJob? job) =>
            _jobs.TryDequeue(out job);

        public void OnEndTick()
        {
            foreach (var job in _jobs)
            {
                job.Wait();
                job.Live();
            }
        }

        public override string ToString() =>
            $"{_jobs.Count}";
    }
}