﻿namespace SimpleQueueSimulator.Lib
{
    public class Job : IJob
    {
        public int LifetimeTicks { get; private set; } = 0;

        public int BlockTicks { get; private set; } = 0;

        public int WaitTicks { get; private set; } = 0;

        public int ProcessTicks { get; private set; } = 0;

        public bool IsDiscarded { get; private set; } = false;
        
        public void Discard() =>
            IsDiscarded = true;

        public void Block() =>
            ++BlockTicks;

        public void Wait() =>
            ++WaitTicks;

        public void Process() =>
            ++ProcessTicks;

        public void Live() =>
            ++LifetimeTicks;
    }
}