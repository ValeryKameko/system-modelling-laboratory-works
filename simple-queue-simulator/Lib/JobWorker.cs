﻿using System.Collections.Generic;
using System.Linq;

namespace SimpleQueueSimulator.Lib
{
    public abstract class JobWorker : IQueueSystemElement
    {
        private IJob? _workJob = null;
        public IEnumerable<IJob> Jobs => _workJob == null ? Enumerable.Empty<IJob>() : new[] {_workJob};

        public JobWorker()
        {
        }

        public bool CanInsert() =>
            _workJob == null;

        public virtual bool TryInsert(IJob job)
        {
            var insert = CanInsert();
            if (insert)
                _workJob = job;
            return insert;
        }

        public virtual bool CanExtract() => 
            _workJob != null;

        public abstract bool TryExtract(out IJob? job);

        public virtual void OnEndTick() {
            _workJob?.Process();
            _workJob?.Live();
        }

        protected IJob? ExtractJob()
        {
            var extractedJob = _workJob;
            _workJob = null;
            return extractedJob;
        }
    }
}