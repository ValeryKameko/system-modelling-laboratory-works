﻿using System;
using System.Collections.Generic;
using SimpleQueueSimulator.Lib;
using SimpleQueueSimulator.Lib.JobSources;
using SimpleQueueSimulator.Lib.JobWorkers;

namespace SimpleQueueSimulator
{
    class Program
    {
        public static int TICKS_COUNT = 1000000;

        static void Main(string[] args)
        {
            Console.Out.Write("Количество тактов (N) = ");
            int ticksCount = int.Parse(Console.ReadLine());
            Console.Out.Write("Вероятность отсеивания канала обслуживания 1 (pi1) = ");
            var pi1 = double.Parse(Console.ReadLine());
            Console.Out.Write("Вероятность отсеивания канала обслуживания 1 (pi1) = ");
            var pi2 = double.Parse(Console.ReadLine());
            
            
            var queueSystem = new QueueSystem();

            var random = new Random(123);

            var jobSource = new FixedTicksJobSource(2, () => new Job());
            var queue = new JobQueue(2);
            var jobWorker1 = new SiftedJobWorker(random, pi1);
            var jobWorker2 = new SiftedJobWorker(random, pi2);

            foreach (var element in new IQueueSystemElement[] {jobSource, queue, jobWorker1, jobWorker2})
                queueSystem.InsertElement(element);

            queueSystem.InsertEdge(new QueueSystemElementEdge(QueueSystemElementEdge.EdgeType.Block, jobSource, queue));
            queueSystem.InsertEdge(new QueueSystemElementEdge(QueueSystemElementEdge.EdgeType.Block, queue,
                jobWorker1));
            queueSystem.InsertEdge(new QueueSystemElementEdge(QueueSystemElementEdge.EdgeType.Block, queue,
                jobWorker2));

            var jobStat = new JobStat();

            var hshs = new Dictionary<String, int>();
            var hshs2 = new Dictionary<String, int>();
            ;
            var prev = "";
            for (var i = 0; i < ticksCount; i++)
            {
                var curr = $"{jobSource}{queue}{jobWorker1}{jobWorker2}";
                
                hshs.TryAdd(curr, 0);
                hshs[curr]++;
                
                hshs2.TryAdd(prev + "->" + curr, 0);
                hshs2[prev + "->" + curr]++;

                queueSystem.ProcessTick(jobStat);
                prev = curr;
            }

            foreach (var (key, cnt) in hshs)
            {
                Console.Out.WriteLine($"P{key} = {cnt / (double) TICKS_COUNT}");
            }

            Console.Out.WriteLine($"Вероятность блокировки (Pбл) = {jobStat.BlockProbability}");
            Console.Out.WriteLine($"Средняя длина очереди (Lоч) = {jobStat.AverageQueueLength}");
            Console.Out.WriteLine($"Среднее число заявок, находящихся в системе (Lс) = {jobStat.AverageJobsCount}");
            Console.Out.WriteLine($"Абсолютная пропускная способность (A) = {jobStat.AverageThroughput}");
            Console.Out.WriteLine($"Среднее время пребывания заявки в системе (Wс) = {jobStat.AverageWaitTime}");
            Console.Out.WriteLine($"Среднее время пребывания заявки в очереди (Wоч) = {jobStat.AverageLifetimeTime}");
        }
    }
}