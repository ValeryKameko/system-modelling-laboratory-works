use rand::Rng;

use crate::util::sample_time;

pub struct JobSource {
    delay: f64,
    average_delay: f64,
}

impl JobSource {
    const EPSILON: f64 = 1e-4;

    pub fn new(average_delay: f64) -> Self {
        Self {
            delay: sample_time(average_delay),
            average_delay,
        }
    }

    pub fn process_time(&mut self, time: f64) -> bool {
        self.delay -= time;
        if self.delay <= Self::EPSILON {
            self.delay = sample_time(self.average_delay);
            true
        } else {
            false
        }
    }

    pub fn delay(&self) -> f64 {
        self.delay
    }
}
