use rand::{random, Rng};

use crate::util::sample_time;

#[derive(Copy, Clone)]
pub enum WorkerState {
    Busy { work_time: f64 },
    Free,
}

#[derive(Clone)]
pub struct Worker {
    worker_state: WorkerState,
    cost_per_hour: f64,
    average_processing_time: f64,
}

impl Worker {
    const EPSILON: f64 = 1e-4;

    pub fn new(average_processing_time: f64, cost_per_hour: f64) -> Self {
        Self {
            average_processing_time,
            cost_per_hour,
            worker_state: WorkerState::Free,
        }
    }

    pub fn start_work(&mut self) -> bool {
        let work_time = sample_time(self.average_processing_time);
        if let WorkerState::Free = self.worker_state {
            self.worker_state = WorkerState::Busy { work_time };
            true
        } else {
            false
        }
    }

    pub fn process_time(&mut self, time: f64) -> bool {
        if let WorkerState::Busy { work_time } = &mut self.worker_state {
            *work_time -= time;
            *work_time <= Self::EPSILON
        } else {
            false
        }
    }

    pub fn end_work(&mut self) {
        self.worker_state = WorkerState::Free;
    }

    pub fn work_time(&self) -> f64 {
        match self.worker_state {
            WorkerState::Busy { work_time } => work_time,
            WorkerState::Free => f64::MAX,
        }
    }

    pub fn cost_per_hour(&self) -> f64 {
        self.cost_per_hour
    }

    pub fn is_free(&self) -> bool {
        match self.worker_state {
            WorkerState::Busy { .. } => false,
            WorkerState::Free => true,
        }
    }
}
