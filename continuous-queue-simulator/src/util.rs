use rand::Rng;

pub fn sample_time(average_delay: f64) -> f64 {
    let mut rng = rand::thread_rng();
    -(rng.gen_range(0.0..1.0) as f64).log(std::f64::consts::E) * average_delay
}
