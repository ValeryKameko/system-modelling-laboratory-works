use std::io;
use std::str::FromStr;

use anyhow::anyhow;

use crate::model::{Model, ModelConfig};

pub mod job_source;
pub mod model;
pub mod util;
pub mod worker;

pub fn read_value<T: FromStr>(prompt: &str) -> anyhow::Result<T> {
    println!("{}", prompt);

    let mut line = String::new();
    io::stdin()
        .read_line(&mut line)
        .map_err(|err| anyhow!("Пустой ввод"))?;

    let value = line
        .trim()
        .parse::<T>()
        .map_err(|err| anyhow!("Некорректное значение"))?;
    Ok(value)
}

fn try_read_config() -> anyhow::Result<ModelConfig> {
    let job_intensity =
        read_value::<f64>("Введите интенсивность потока заявок (заявок/ч.): ".into())?;

    let average_work_time =
        read_value::<f64>("Введите среднее время обслуживания одной заявки (ч.): ".into())?;

    let cost_per_job = read_value::<f64>("Введите доход от обслуженной заявки (у.е.): ".into())?;

    let cost_per_channel =
        read_value::<f64>("Введите стоимость содержание одного канала (у.е./ч.): ".into())?;

    let workers_count = read_value::<usize>("Введите количество каналов СМО (ед.): ".into())?;

    let config = ModelConfig {
        average_job_delay: 1.0 / job_intensity,
        average_work_time,
        cost_per_hour: cost_per_channel,
        job_cost: cost_per_job,
        worker_count: workers_count,
    };
    Ok(config)
}

fn read_config() -> ModelConfig {
    let mut config = try_read_config();
    while let Err(error) = &config {
        eprintln!("Ошибка ввода.");
        eprintln!();
        config = try_read_config();
    }
    config.unwrap()
}

fn read_time() -> f64 {
    let prompt = "Ввежите время моделирования (ч.): ".into();
    let mut time = read_value(prompt);
    while let Err(error) = &time {
        eprintln!("Ошибка ввода.");
        eprintln!();
        time = read_value(prompt);
    }
    time.unwrap()
}

fn main() {
    let config = read_config();
    let mut model = Model::new(config);
    let time = read_time();

    let stat = model.process(time);
    stat.print_info()
}
