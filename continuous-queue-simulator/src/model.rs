use crate::job_source::JobSource;
use crate::worker::Worker;

#[derive(Clone)]
pub struct ModelConfig {
    pub average_job_delay: f64,
    pub average_work_time: f64,
    pub cost_per_hour: f64,
    pub job_cost: f64,
    pub worker_count: usize,
}

#[derive(Default)]
pub struct ModelStat {
    cost: f64,
    processed_time: f64,
    rejected_jobs_count: usize,
    processed_jobs_count: usize,
    generated_jobs_count: usize,
}

impl ModelStat {
    pub fn print_info(&self) {
        println!(
            "Вероятность отказа: {}",
            self.rejected_jobs_count as f64 / self.generated_jobs_count as f64
        );
        println!(
            "Относительная пропускная способность: {}",
            self.processed_jobs_count as f64 / self.generated_jobs_count as f64
        );
        println!(
            "Абсолютная пропускная способность: {}",
            self.processed_jobs_count as f64 / self.processed_time
        );
        println!(
            "Средний доход (у.е./ч.): {}",
            self.cost / self.processed_time
        );
    }
}

pub struct Model {
    job_cost: f64,
    workers: Vec<Worker>,
    job_source: JobSource,
}

impl Model {
    pub fn new(
        ModelConfig {
            average_job_delay,
            average_work_time,
            cost_per_hour,
            job_cost,
            worker_count,
        }: ModelConfig,
    ) -> Self {
        let workers = vec![Worker::new(average_work_time, cost_per_hour); worker_count];
        let job_source = JobSource::new(average_job_delay);

        Self {
            job_cost,
            workers,
            job_source,
        }
    }

    pub fn process(&mut self, mut time: f64) -> ModelStat {
        let mut stat = ModelStat::default();

        let mut current_time = 0.0;

        while current_time < time {
            let delta_time = self.find_delta_time();
            stat.processed_time += delta_time;
            current_time += delta_time;

            // eprint!("Time {}: ", current_time);

            for (index, mut worker) in self.workers.iter_mut().enumerate() {
                let job_processed = worker.process_time(delta_time);
                if job_processed {
                    // eprintln!("Processed job: worker {}", index);

                    stat.processed_jobs_count += 1;
                    stat.cost += self.job_cost;

                    worker.end_work();
                }

                stat.cost -= delta_time * worker.cost_per_hour();
            }

            let job_generated = self.job_source.process_time(delta_time);

            if job_generated {
                // eprintln!("Generated job");

                stat.generated_jobs_count += 1;
                if let Some((index, worker)) = self
                    .workers
                    .iter_mut()
                    .enumerate()
                    .find(|(_, worker)| worker.is_free())
                {
                    // eprintln!("Processing job: worker {}", index);
                    worker.start_work();
                } else {
                    // eprintln!("Rejected job");
                    stat.rejected_jobs_count += 1;
                }
            }
        }
        stat
    }

    fn find_delta_time(&self) -> f64 {
        let source_delay = self.job_source.delay();
        let work_time = self
            .workers
            .iter()
            .map(|worker| worker.work_time())
            .fold(f64::MAX, f64::min);

        f64::min(source_delay, work_time)
    }
}
