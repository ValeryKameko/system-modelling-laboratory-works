(ns algorithms.distributions.erlang-distribution
  (:require [algorithms.core :refer [factorial binary-search?]]
            [algorithms.distributions.random-distribution :as random-distribution :refer [RandomDistribution]]
            [algorithms.distributions.base-values :as base-values]))

(deftype ErlangDistribution []
  RandomDistribution
  (generate [this generator parameters]
    (let [eta (:eta parameters)
          lambda (:lambda parameters)]
      (->> (repeatedly eta generator)
           (map Math/log)
           (reduce + 0)
           (#(/ % lambda))
           (#(- %)))))
  (get-info [this]
    {:name "Erlang distribution"})
  (get-parameter-ranges [this parameters]
    {:eta [1 50]
     :lambda base-values/float-exponential-limits})
  (get-parameter-infos [this]
    {:eta
      {:type "integer"
       :name "η"
       :default 1
       :precision 3
       :step-amount 1}
     :lambda
      {:type "decimal"
       :name "λ"
       :default 1
       :precision 3
       :step-amount 0.5}})
  (get-value [this value parameters]
    (let [eta (:eta parameters)
          lambda (:lambda parameters)]
      (*
        (Math/pow lambda eta)
        (/ 1 (factorial (dec eta)))
        (Math/pow value (dec eta))
        (Math/exp (- (* lambda value))))))
  (get-range [this parameters]
    (let [eta (:eta parameters)
          lambda (:lambda parameters)
          mean (/ eta lambda)
          mean-value (random-distribution/get-value this mean parameters)
          high (binary-search?
                  (* mean-value base-values/range-min-value)
                  base-values/precision
                  #(random-distribution/get-value this % parameters)
                  mean
                  (second base-values/range-search-limits))]
      [0 high]))
  (get-estimations [this parameters]
    (let [eta (:eta parameters)
          lambda (:lambda parameters)]
      {:m (/ eta lambda)
       :D (/ eta lambda lambda)
       :sigma (/ (Math/sqrt eta) lambda)})))