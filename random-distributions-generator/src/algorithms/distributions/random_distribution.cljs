(ns algorithms.distributions.random-distribution)

(defprotocol RandomDistribution
  (generate [this generator parameters])
  (get-info [this])
  (get-parameter-ranges [this parameters])
  (get-parameter-infos [this])
  (get-value [this value parameters])
  (get-range [this parameters])
  (get-estimations [this parameters]))
