(ns algorithms.distributions.uniform-distribution
  (:require [algorithms.distributions.random-distribution :refer [RandomDistribution]]
            [algorithms.distributions.base-values :refer [float-limits]]))

(deftype UniformDistribution []
  RandomDistribution
  (generate [this generator parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      (-> (generator) (* (- b a)) (+ a))))
  (get-info [this]
    {:name "Uniform distribution"})
  (get-parameter-ranges [this parameters]
    (let [a (or (:a parameters) (first float-limits))
          b (or (:b parameters) (second float-limits))]
      {:a [(first float-limits) b]
       :b [a (second float-limits)]}))
  (get-parameter-infos [this]
    {:a {:type "decimal"
         :name "a"
         :default 0
         :precision 3
         :step-amount 0.5}
     :b {:type "decimal"
         :name "b"
         :default 1
         :precision 3
         :step-amount 0.5}})
  (get-value [this value parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      (if (and (< a value) (< value b))
        (/ 1 (- b a))
        0)))
  (get-range [this parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      [a b]))
  (get-estimations [this parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      {:m (/ (+ a b) 2)
       :D (/ (Math/pow (- b a) 2) 12)
       :sigma (/ (- b a) (Math/sqrt 12))})))