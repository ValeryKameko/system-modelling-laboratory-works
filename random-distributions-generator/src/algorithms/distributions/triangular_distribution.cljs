(ns algorithms.distributions.triangular-distribution
  (:require [algorithms.core :refer [factorial binary-search?]]
            [algorithms.distributions.random-distribution :as random-distribution :refer [RandomDistribution]]
            [algorithms.distributions.base-values :as base-values]))

(deftype RightTriangularDistribution []
  RandomDistribution
  (generate [this generator parameters]
    (let [a (:a parameters)
          b (:b parameters)
          r1 (generator)
          r2 (generator)]
      (if (< r1 r2)
        (-> r1 (* (- b a)) (+ a))
        (random-distribution/generate this generator parameters))))
  (get-info [this]
    {:name "Right triangular distribution"})
  (get-parameter-ranges [this parameters]
    (let [a (or (:a parameters) (first base-values/float-limits))
          b (or (:b parameters) (second base-values/float-limits))]
      {:a [(first base-values/float-limits) b]
       :b [a (second base-values/float-limits)]}))
  (get-parameter-infos [this]
    {:a {:type "decimal"
         :name "a"
         :default 0
         :precision 3
         :step-amount 0.5}
     :b {:type "decimal"
         :name "b"
         :default 1
         :precision 3
         :step-amount 0.5}})
  (get-value [this value parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      (if (and (< a value) (< value b))
        (/ (* 2 (- value a))
           (Math/pow (- b a) 2))
        0)))
  (get-range [this parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      [a b]))
  (get-estimations [this parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      {:m (/ (+ a a b) 3)
       :D (-> (- b a) (Math/pow 2) (/ 18))
       :sigma (-> (- b a) (/ (Math/sqrt 18)))})))

(deftype LeftTriangularDistribution []
  RandomDistribution
  (generate [this generator parameters]
    (let [a (:a parameters)
          b (:b parameters)
          r1 (generator)
          r2 (generator)]
      (if (> r2 (- 1 r1))
        (-> r1 (* (- b a)) (+ a))
        (random-distribution/generate this generator parameters))))
  (get-info [this]
    {:name "Left triangular distribution"})
  (get-parameter-ranges [this parameters]
    (let [a (or (:a parameters) (first base-values/float-limits))
          b (or (:b parameters) (second base-values/float-limits))]
      {:a [(first base-values/float-limits) b]
       :b [a (second base-values/float-limits)]}))
  (get-parameter-infos [this]
    {:a {:type "decimal"
         :name "a"
         :default 0
         :precision 3
         :step-amount 0.5}
     :b {:type "decimal"
         :name "b"
         :default 1
         :precision 3
         :step-amount 0.5}})
  (get-value [this value parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      (if (and (< a value) (< value b))
        (/ (* 2 (- b value))
           (Math/pow (- b a) 2))
        0)))
  (get-range [this parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      [a b]))
  (get-estimations [this parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      {:m (/ (+ a b b) 3)
       :D (-> (- b a) (Math/pow 2) (/ 18))
       :sigma (-> (- b a) (/ (Math/sqrt 18)))})))
