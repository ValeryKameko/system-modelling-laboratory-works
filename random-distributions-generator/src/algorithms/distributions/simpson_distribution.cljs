(ns algorithms.distributions.simpson-distribution
  (:require [algorithms.core :refer [factorial binary-search?]]
            [algorithms.distributions.random-distribution :as random-distribution :refer [RandomDistribution]]
            [algorithms.distributions.base-values :as base-values]))

(deftype SimpsonDistribution []
  RandomDistribution
  (generate [this generator parameters]
    (let [a (:a parameters)
          b (:b parameters)
          rs (repeatedly 2 #(-> (generator) (* (- b a)) (+ a) (/ 2)))]
      (reduce + rs)))
  (get-info [this]
    {:name "Simpson distribution"})
  (get-parameter-ranges [this parameters]
    (let [a (or (:a parameters) (first base-values/float-limits))
          b (or (:b parameters) (second base-values/float-limits))]
      {:a [(first base-values/float-limits) b]
       :b [a (second base-values/float-limits)]}))
  (get-parameter-infos [this]
    {:a {:type "decimal"
         :name "a"
         :default 0
         :precision 3
         :step-amount 0.5}
     :b {:type "decimal"
         :name "b"
         :default 1
         :precision 3
         :step-amount 0.5}})
  (get-value [this value parameters]
    (let [a (:a parameters)
          b (:b parameters)
          middle (/ (+ a b) 2)]
      (cond
        (and (< a value) (< value middle)) (-> (- value a) (* 4) (/ (Math/pow (- b a) 2)))
        (and (< middle value) (< value b)) (-> (- b value) (* 4) (/ (Math/pow (- b a) 2)))
        :else 0)))
  (get-range [this parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      [a b]))
  (get-estimations [this parameters]
    (let [a (:a parameters)
          b (:b parameters)]
      {:m (/ (+ a b) 2)
       :D (/ (Math/pow (- b a) 2) 24)
       :sigma (/ (- b a) (Math/sqrt 24))})))