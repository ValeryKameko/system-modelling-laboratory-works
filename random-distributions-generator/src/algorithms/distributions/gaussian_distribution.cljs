(ns algorithms.distributions.gaussian-distribution
  (:require [algorithms.core :refer [binary-search?]]
            [algorithms.distributions.random-distribution :as random-distribution :refer [RandomDistribution]]
            [algorithms.distributions.base-values :as base-values]))

(deftype GaussianDistribution []
  RandomDistribution
  (generate [this generator parameters]
    (let [m (:m parameters)
          sigma (:sigma parameters)
          n (:n parameters)
          rs (repeatedly n generator)]
      (->> rs
           (reduce +)
           (#(- % (/ n 2)))
           (* (Math/sqrt (/ 12 n)))
           (* sigma)
           (+ m))))
  (get-info [this]
    {:name "Gaussian distribution"})
  (get-parameter-ranges [this parameters]
    {:m base-values/float-limits
     :sigma [0 (second base-values/float-limits)]
     :n [1 20]})
  (get-parameter-infos [this]
    {:m 
      {:type "decimal"
       :name "m"
       :default 0
       :precision 3
       :step-amount 0.01}
     :sigma 
      {:type "decimal"
       :name "σ"
       :default 1
       :precision 3
       :step-amount 0.01}
     :n
      {:type "integer"
       :name "n"
       :default 6
       :step-amount 1}})
  (get-value [this value parameters]
    (let [m (:m parameters)
          sigma (:sigma parameters)]
      (/ (-> value (- m) (/ sigma) (Math/pow 2) (/ -2) (Math/exp))
         (Math/sqrt (* 2 Math/PI ))
         sigma)))
  (get-range [this parameters]
    (let [m (:m parameters)
          sigma (:sigma parameters)
          m-value (random-distribution/get-value this m parameters)
          low (binary-search?
                (* m-value base-values/range-min-value)
                base-values/precision
                #(random-distribution/get-value this % parameters)
                m
                (first base-values/range-search-limits))
          high (binary-search?
                  (* m-value base-values/range-min-value)
                  base-values/precision
                  #(random-distribution/get-value this % parameters)
                  m
                  (second base-values/range-search-limits))]
      [low high]))
  (get-estimations [this parameters]
    (let [m (:m parameters)
          sigma (:sigma parameters)]
      {:m m
       :D (Math/pow sigma 2) 
       :sigma sigma})))
    