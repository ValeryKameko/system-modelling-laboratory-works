(ns algorithms.distributions.exponential-distribution
  (:require [algorithms.core :refer [factorial binary-search?]]
            [algorithms.distributions.random-distribution :as random-distribution :refer [RandomDistribution]]
            [algorithms.distributions.base-values :as base-values]))

(deftype ExponentialDistribution []
  RandomDistribution
  (generate [this generator parameters]
    (let [lambda (:lambda parameters)]
      (-> (generator) Math/log (/ lambda) (* -1))))
  (get-info [this]
    {:name "Exponential distribution"})
  (get-parameter-ranges [this parameters]
    {:lambda base-values/float-exponential-limits})
  (get-parameter-infos [this]
    {:lambda {:type "decimal"
              :name "λ"
              :default 1
              :precision 3
              :step-amount 0.01}})
  (get-value [this value parameters]
    (let [lambda (:lambda parameters)]
      (* lambda
         (Math/exp (- (* lambda value))))))
  (get-range [this parameters]
    (let [lambda (:lambda parameters)
          mean (/ 1 lambda)
          mean-value (random-distribution/get-value this mean parameters)
          high (binary-search?
                  (* mean-value base-values/range-min-value)
                  base-values/precision
                  #(random-distribution/get-value this % parameters)
                  mean
                  (second base-values/range-search-limits))]
      [0 high]))
  (get-estimations [this parameters]
    (let [lambda (:lambda parameters)]
      {:m (/ 1 lambda)
       :D (/ 1 lambda lambda)
       :sigma (/ 1 lambda)})))
    