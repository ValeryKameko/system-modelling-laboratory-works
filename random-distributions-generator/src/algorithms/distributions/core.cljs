(ns algorithms.distributions.core
  (:require [algorithms.distributions.random-distribution :as random-distribution]
            [algorithms.distributions.uniform-distribution :refer [UniformDistribution]]
            [algorithms.distributions.exponential-distribution :refer [ExponentialDistribution]]
            [algorithms.distributions.gaussian-distribution :refer [GaussianDistribution]]
            [algorithms.distributions.erlang-distribution :refer [ErlangDistribution]]
            [algorithms.distributions.triangular-distribution :refer [RightTriangularDistribution LeftTriangularDistribution]]
            [algorithms.distributions.simpson-distribution :refer [SimpsonDistribution]]))

(def distributions 
  {:uniform-distribution (UniformDistribution.)
   :exponential-distribution (ExponentialDistribution.)
   :gaussian-distribution (GaussianDistribution.)
   :erlang-distribution (ErlangDistribution.)
   :right-triangular-distribution (RightTriangularDistribution.)
   :left-triangular-distribution (LeftTriangularDistribution.)
   :simpson-distribution (SimpsonDistribution.)})