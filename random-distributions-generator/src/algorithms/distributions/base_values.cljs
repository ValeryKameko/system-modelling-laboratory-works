(ns algorithms.distributions.base-values)

(def float-limits [-10000 10000])
(def float-exponential-limits [1.0e-3 1.0e+3])
(def int-limits [-10000 10000])
(def precision 1.0e-5)
(def range-min-value 1.0e-3)
(def range-search-limits [-1.0e+10 1.0e+10])