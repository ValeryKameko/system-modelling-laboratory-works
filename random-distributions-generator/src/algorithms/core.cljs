(ns algorithms.core)

(defn factorial [n]
  (reduce * (range 1 (inc n))))

(defn binary-search? [value precision func low high]
  (let [middle (/ (+ high low) 2)
        middle-value (func middle)]
    (cond 
      (< (Math/abs (- high low)) precision) middle
      (< middle-value value) (binary-search? value precision func low middle)
      :else (binary-search? value precision func middle high))))