(ns algorithms.random-estimators)

(defn calc-sum [xs]
    (reduce + 0 xs))

(defn calc-mean [xs]
  (/ (calc-sum xs) (count xs)))

(defn calc-dispersion [xs]
  (let [mean (calc-mean xs)
        n (count xs)
        biased-dispersion (->> xs 
                               (map #(- % mean))
                               (map #(* % %))
                               calc-mean)]
    (if (< n 2)
      biased-dispersion
      (-> biased-dispersion 
          (* n) 
          (/ (- n 1))))))

(defn calc-standard-deviation [xs]
  (->> xs calc-dispersion Math/sqrt))
