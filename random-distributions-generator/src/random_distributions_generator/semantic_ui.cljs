(ns random-distributions-generator.semantic-ui
  (:require [cljsjs.semantic-ui-react]
            [goog.object :as go]
            [clojure.string :as str]))

(def semantic-ui js/semanticUIReact)

(def NumberInput (-> (js/require "semantic-ui-react-numberinput") .-default))

(defn component [key]
  (let [keys (str/split key #"\.")]
    (apply go/getValueByKeys semantic-ui keys)))
