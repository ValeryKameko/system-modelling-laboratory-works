(ns random-distributions-generator.renderer
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.dom :as rdom]
            [reagent.format :as fmt]
            ["react" :as react]
            [random-distributions-generator.semantic-ui :as sui]
            [random-distributions-generator.charts :refer [histogram-component]]
            [random-distributions-generator.store :as store]))

(def float-format "%.10f")
(def int-format "%d")

(defn- format-number [format x]
  (or
    (some->> x (fmt/format format))
    "—"))

(defn- parameter-component [parameter info]
  ^{:key parameter}
  [:> (sui/component "Form")
    [:> (sui/component "Form.Field")
      [:label (:name info)]
      [:> sui/NumberInput {:value (store/get-parameter-value parameter)
                           :onChange #(store/set-parameter-value parameter %)
                           :minValue (some-> (store/get-parameter-range parameter) first)
                           :maxValue (some-> (store/get-parameter-range parameter) second)
                           :valueType (:type info)
                           :allowMouseWheel true
                           :precision (:precision info)
                           :stepAmount (:step-amount info)
                           :size "mini"}]]])

(defn- get-distribution-options []
  (map 
    (fn [[distribution distribution-info]]
      {:key (:name distribution-info)
       :text (:name distribution-info)
       :value (name distribution)})
    (store/get-distribution-infos)))

(defn- distributions-choice-component []
  [:> (sui/component "Dropdown") {:selection true
                                  :fluid true
                                  :options (clj->js (get-distribution-options))
                                  :onChange #(-> %2 .-value keyword store/set-distribution)
                                  :defaultValue (store/get-distribution-id)}])
            
(defn- controls-component []
  (let [parameter-components (map #(parameter-component (first %) (second %)) (store/get-parameter-infos))]
    [:> (sui/component "Segment.Group") {:raised true}
      [:> (sui/component "Segment")
        [:> (sui/component "Header") {:as "h3"
                                      :textAlign "center"}
         "Controls"]]
      [:> (sui/component "Segment")
        [:> (sui/component "Header") {:as "h4"}
          "Distribution"]
        [distributions-choice-component (get-distribution-options)]]
      [:> (sui/component "Segment")
        [:> (sui/component "Header") {:as "h4"}
          "Distribution parameters"]
        (doall parameter-components)]
      [:> (sui/component "Segment")
        [:> (sui/component "Header") {:as "h4"}
          "Generating parameters"]
        [:> (sui/component "Form")
          [:> (sui/component "Form.Field")
            [:label "N (count)"]
            [:> sui/NumberInput {:value (str (store/get-N))
                                 :onChange #(store/set-N %)
                                 :minValue (first store/N-range)
                                 :maxValue (second store/N-range)
                                 :valueType "integer"
                                 :size "mini"}]]]]
      [:> (sui/component "Segment") {:textAlign "center"}
        [:> (sui/component "Button") {:onClick store/generate-xs
                                      :disabled (store/get-generating)
                                      :loading (store/get-generating)}
          "Generate"]]]))

(defn- estimations-component [header estimations]
  [:> (sui/component "Segment.Group") {:raised true}
    [:> (sui/component "Segment")
      [:> (sui/component "Header") {:as "h4"
                                    :textAlign "center"}
        header]]
    [:> (sui/component "Segment")
      [:> (sui/component "Table")
        [:> (sui/component "Table.Body")
          [:> (sui/component "Table.Row")
            [:> (sui/component "Table.Cell") {:width 1}
              [:> (sui/component "Header") {:as "h5"
                                            :textAlign "center"}
                "m (mean)"]]
            [:> (sui/component "Table.Cell") {:textAlign "center"
                                              :width 5}
              (format-number float-format (some->> estimations :m))]]
          [:> (sui/component "Table.Row")
            [:> (sui/component "Table.Cell") {:width 1}
              [:> (sui/component "Header") {:as "h5"
                                            :textAlign "center"}
                "D (dispersion)"]]
            [:> (sui/component "Table.Cell") {:textAlign "center"
                                              :width 5}
              (format-number float-format (some->> estimations :D))]]
          [:> (sui/component "Table.Row")
            [:> (sui/component "Table.Cell") {:width 1}
              [:> (sui/component "Header") {:as "h5"
                                            :textAlign "center"}
                "σ (standard deviation)"]]
            [:> (sui/component "Table.Cell") {:textAlign "center"
                                              :width 5}
              (format-number float-format (some->> estimations :sigma))]]]]]])

(defn root-component []
  [:div {:style {:margin "16px"}}
    [:> (sui/component "Grid") {:columns 2}
      [:> (sui/component "Grid.Row")
        [:> (sui/component "Grid.Column") {:width 12}
          [histogram-component (store/get-xs)
                               (store/get-min)
                               (store/get-max)
                               (store/get-bars-count)]]
        [:> (sui/component "Grid.Column") {:width 4}
          [controls-component]
          [estimations-component "Estimations" (store/get-estimations)]
          [estimations-component "Distribution estimations" (store/get-distribution-estimations)]
          ]]]])

(rdom/render [root-component]
             (js/document.getElementById "app-container"))
