(ns random-distributions-generator.store
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.dom :as rdom]
            [cljs.core.async :refer [<!]]
            [algorithms.random-estimators :refer [calc-mean calc-dispersion calc-standard-deviation]]
            [algorithms.distributions.random-distribution :as random-distribution]
            [algorithms.distributions.core :refer [distributions]])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(def ^:const N-range [0 100000])

(def store (atom 
  {:parameters nil 
   :distribution-pair (-> distributions first)
   :generating-parameters {:N 10}
   :data {:xs []}
   :estimations nil
   :ui {:bars-count 20
        :generating false}}))

(defn get-estimations []
  (-> @store :estimations))

(defn set-estimations [estimations]
  (swap! store assoc :estimations estimations))

(defn set-xs [xs]
  (.log js/console "Generated")
  (.log js/console (clj->js xs))
  (swap! store assoc-in [:data :xs] xs))

(defn get-xs []
  (-> @store :data :xs))

(defn on-change-parameter []
  (set-xs nil)
  (set-estimations nil))

(defn set-distribution [distribution-id]
  (let [distribution (find distributions distribution-id)]
    (on-change-parameter)
    (swap! store assoc :distribution-pair distribution)))

(defn get-distribution []
  (-> @store :distribution-pair second))

(defn get-distribution-id []
  (-> @store :distribution-pair first))

(defn get-distribution-infos []
  (into {} 
    (for [[k v] distributions] 
      [k (random-distribution/get-info v)])))

(defn get-parameter-infos []
  (-> (get-distribution) random-distribution/get-parameter-infos))

(defn get-parameters []
  (-> @store :parameters))

(defn set-parameter-value [parameter value]
  (on-change-parameter)
  (swap! store assoc-in [:parameters parameter] value))

(defn get-parameter-value [parameter]
  (let [info (parameter (get-parameter-infos))]
    (if-let [value (-> @store :parameters parameter)]
      value
      (if-let [parameter-default (:default info)]
        (do
          (set-parameter-value parameter parameter-default)
          parameter-default)))))

(defn- parse-parameter-value [parameter info]
  (let [parameter-value (get-parameter-value parameter)
        parameter-type (:type info)
        parameter-default (:default info)]
    (or 
      (case parameter-type
        "decimal" (some-> parameter-value (js/parseFloat))
        "integer" (some-> parameter-value (js/parseInt))
        nil)
      parameter-default)))

(defn- get-parsed-parameters []
  (let [parameters (get-parameter-infos)]
    (into {} (for [[k v] parameters] [k (parse-parameter-value k v)]))))

(defn get-distribution-estimations []
  (let [distribution (get-distribution)
        parameters (get-parsed-parameters)]
    (random-distribution/get-estimations distribution parameters)))

(defn get-parameter-range [parameter]
  (let [parameters (get-parsed-parameters)
        parameter-ranges (-> (get-distribution) (random-distribution/get-parameter-ranges parameters))]
    (parameter parameter-ranges)))

(defn get-min []
  (or (some-> (get-distribution) (random-distribution/get-range (get-parsed-parameters)) first) 0.0))

(defn get-max []
  (or (some-> (get-distribution) (random-distribution/get-range (get-parsed-parameters)) second) 1.0))

(defn set-N [N]
  (on-change-parameter)
  (swap! store assoc-in [:generating-parameters :N] N))

(defn get-N []
  (-> @store :generating-parameters :N))

(defn get-bars-count []
  (-> @store :ui :bars-count))

(defn set-bars-count [bars-count]
  (swap! store assoc-in [:ui :bars-count] bars-count))

(defn get-generating []
  (-> @store :ui :generating))

(defn set-generating [generating]
  (swap! store assoc-in [:ui :generating] generating))

(defn- calc-estimations [xs]
  (let [mean (calc-mean xs)
        dispersion (calc-dispersion xs)
        standard-deviation (calc-standard-deviation xs)]
    {:m mean
     :D dispersion
     :sigma standard-deviation}))

(defn- generate-values [distribution N parameters]
  (repeatedly N #(random-distribution/generate distribution rand parameters)))

(defn generate-xs []
  (let [parameters (get-parsed-parameters)
        N (js/parseInt (get-N))
        distribution (get-distribution)]
    (set-generating true)
    (go
      (set-xs (generate-values distribution N parameters))
      (-> (get-xs) calc-estimations set-estimations)
      (set-generating false))))
